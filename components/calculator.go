package components

type Calculator struct {
}

func (c Calculator) Plus(a, b float64) float64 {
	return a + b
}

func (c Calculator) Minus(a, b float64) float64 {
	return a - b
}

func (c Calculator) Multiply(a, b float64) float64 {
	return a * b
}

func (c Calculator) Divide(a, b float64) float64 {
	return a / b
}
