package main

import (
	"bufio"
	"errors"
	"fmt"
	"go-calculator/components"
	"os"
	"strconv"
	"strings"
)

func main() {
	reader := bufio.NewReader(os.Stdin)
	fmt.Println("Simple Calculator")
	fmt.Println("-----------------------------------------")
	fmt.Println("Separate number or operation with space")
	fmt.Println("Example: 2 + 3 * 5")

	for {
		fmt.Print("-> ")
		text, _ := reader.ReadString('\n')
		text = strings.Replace(text, "\n", "", -1)
		slice := strings.Split(text, " ")

		slice = CalculateSliceOfString(slice)

		fmt.Println(slice[0])
	}
}

func CalculateSliceOfString(slice []string) []string {
	slice = LoopOperation(slice, "*", "/")
	slice = LoopOperation(slice, "-", "+")
	return slice
}

func LoopOperation(slice []string, operator1 string, operator2 string) []string {
	calculator := components.Calculator{}
	for {
		i, operator, err := Find(slice, operator1, operator2)
		if err != nil {
			break
		} else {
			number1, _ := strconv.ParseFloat(slice[i-1], 64)
			number2, _ := strconv.ParseFloat(slice[i+1], 64)
			var result float64
			switch operator {
			case "*":
				result = calculator.Multiply(number1, number2)
			case "/":
				result = calculator.Divide(number1, number2)
			case "+":
				result = calculator.Plus(number1, number2)
			case "-":
				result = calculator.Minus(number1, number2)
			}
			resultString := fmt.Sprintf("%f", result)
			slice = Insert(slice, i, resultString)
		}
	}
	return slice
}

func Find(a []string, x string, y string) (int, string, error) {
	for i, n := range a {
		if n == x || n == y {
			return i, n, nil
		}
	}
	return -1, "", errors.New("not found")
}

func Insert(slice []string, i int, v string) []string {
	return append(append(slice[:i-1], v), slice[i+2:]...)
}
